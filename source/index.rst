template-website
================

.. warning::

   We are in the process of improving the documentation. You can `test it here <contents.html>`__, and you are very welcome to signal issues with the documentation `on the issue tracker <https://gitlab.com/sat-metalab/documentations/template-website/-/issues>`__.

.. container:: index-locales

   `En <../en/index.html>`__ -=- `Fr <../fr/index.html>`__

.. container:: index-intro

   welcome to the template-website documentation website
  
.. container:: index-menu

   `-` `documentation <contents.html>`__ - `gitlab <https://gitlab.com/sat-metalab/template-website>`__- `about us <https://sat.qc.ca/fr/recherche/metalab>`__ - `get in touch <https://gitlab.com/sat-metalab/template-website/-/issues>`__ -
  

.. container:: index-section

    what is template-website?

template-website is ...
  
.. raw:: html

  <!--- if you wish to include a demo>
    
.. container:: index-section

   how does it works?

well...

.. container:: index-section

   where can I get template-website?

here !
  
.. container:: index-section

   table of contents
   
* how to `install template-website <./install/contents.html>`__
* `a first tutorial <./tutorials/first_steps.html#standard-usages>`__ to get you started
* `tutorials <./tutorials/first_steps.html#advanced-usages>`__ to grow your skills
* `how-to guides <./howto/contents.html>`__ for step-by-step instructions to accomplish specific tasks
* `Frequently Asked Questions (FAQ) <./faq/contents.html>`__ for your troubleshooting needs

  
.. container:: index-section

   please show me the code!
   
For more information visit `the code repository <https://gitlab.com/sat-metalab/template-website>`__.
  
.. container:: index-section

   sponsors

This project is made possible thanks to the `Society for Arts and Technologies <http://www.sat.qc.ca>`__ (also known as SAT).


